CREATE TABLE [dbo].[JNDI_Book](
	[id] [int] NULL,
	[isbn] [varchar](50) NULL,
	[name] [varchar](50) NULL,
	[author] [varchar](50) NULL,
	[publisher] [varchar](50) NULL,
	[price] [int] NULL,
	[Delta] [nchar](10) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JNDI_Stock]    Script Date: 22-04-2019 11:28:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JNDI_Stock](
	[stockID] [int] NULL,
	[stockCode] [varchar](50) NULL,
	[stockName] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[JNDI_User]    Script Date: 22-04-2019 11:28:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JNDI_User](
	[ID] [int] NULL,
	[USER_NAME] [varchar](50) NULL,
	[PASSWORD] [varchar](50) NULL,
	[EMAIL] [varchar](50) NULL,
	[PHONE] [varchar](50) NULL,
	[CITY] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

