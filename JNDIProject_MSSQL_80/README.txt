>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BEGIN<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

******************Please follow below steps carefully to deploy database and run Sample application.

*About Sample Application:

Please extract the ZIP File "JNDI_DB_MSSQL.zip" and follow the below steps:-

**Sample application 'JNDIProject_MSSQL_80.war' is 'JNDI' type sample application written in Java. This application have relationship with 1 databases 'Hibernate_JNDI'. 'Hibernate_JNDI' is MSSQL databases. 

*Requirements to run Sample Application:
 1. Tomcat 8.X must be installed on machine.
 2. MSSQL server must be installed on machine
 3. Download 7-Zip software to extract Sample application war file (To download please follow this link https://www.7-zip.org/download.html)

*Install and Run Application and deploy databases:

>>>>>>Before runn sample application you must run databases scripts (i.e. follow Deploy Database steps.) After that you need to make some changes in configuration files of sample application to pass your database server details to make application connection with database.

**Deploy Database:

  1. Script file is provided inside 'JNDI_DB_MSSQL' folder.
  2. There are 2 scripts inside folder 'JNDI_Create_Database.sql' and 'JNDI_Create_Table.sql'. First execute 'JNDI_Create_Database.sql' to create database and after that execute 'JNDI_Create_Table.sql' to create tables.
  3. Script will create 1 database 'Hibernate_JNDI' with 3 tables 'JNDI_Book', 'JNDI_User' and 'JNDI_Stock' on your database server.

    
**Change 'context.xml' file and provide valid database details to establist connections.   
  1. To make changes in 'conext.xml' file right click on 'JNDIProject_MSSQL_80.war' file and then navigate to '7-Zip' and then on right side menu click on 'Open archive'.
  2. In new window click on 'META-INF' and then click 'context.xml' file.
  3. Replace jdbc connection details for database with your valid databases server details along with username and Password. 


>>>>>For example in Application.properties files you will see below steps:


<Context>
    <Resource name="jdbc/MyLocalDB" 
      global="jdbc/MyLocalDB" 
      auth="Container" 
      type="javax.sql.DataSource" 
      driverClassName="com.microsoft.sqlserver.jdbc.SQLServerDriver" 
      url="jdbc:sqlserver://10.191.74.80:1433;databaseName=Hibernate_JNDI" 
      username="sa" 
      password="sa123456"       
      maxActive="100" 
      maxIdle="20" 
      minIdle="5" 
      maxWait="10000"/>
</Context>


Now you need to change mysql server details (i.e. IP and Port Number). Database username and Password. No need to change database name Hibernate_JNDI.


**Install Application:

  1. This application will run on Apache Tomcat server. (To run Tomcat you must have Java 8 installed on machine.)
  2. Copy 'JNDIProject_MSSQL_80.war' file and paste it in Webapps folder of Apache Tomcat server.
  3. Run Tomcat 8 server. 

  
**Verify application is running or not?

  1. To do this you need to below URL in your browser:
  
     http://{your IP Address}:Port Number on which Tomcat is running/JPAPersistenceProject/
  
     For example in my case my tomcat is running on port 8080 and my machine IP was 10.191.74.90. So, to ensure application is running I hit below URL in browser: 
   
     http://10.191.74.90:8080/JNDIProject_MSSQL_80/

  2. Enter values in UI and click on 'Submit' button. Please make sure enter PersonId is numeric number.




>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>END<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  


