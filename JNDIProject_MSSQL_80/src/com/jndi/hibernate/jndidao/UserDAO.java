package com.jndi.hibernate.jndidao;                      
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.jndi.hibernate.jndientity.User;

public class UserDAO {
	public UserDAO() {
	}
	public void addUserDetails(String userName, String password, String email,String city,SessionFactory sessionFactory) {
		try {
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			User user = new User();
			user.setUserName(userName);
			user.setPassword1(password);
			user.setEmail(email);
			user.setCity(city);
			session.saveOrUpdate(user);
			transaction.commit();
			System.out.println("\n\n Details Added \n");
			session.close();
		} catch (HibernateException e) {
			System.out.println(e.getMessage());
			System.out.println("error");
		}
	}
}
