package com.boraji.tutorial.spring;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import com.boraji.tutorial.spring.config.AppConfig;
import com.boraji.tutorial.spring.entity.Person;
import com.boraji.tutorial.spring.service.PersonService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * @author ankit.tripathi
 *
 */

@EnableTransactionManagement
@ComponentScans(value = { @ComponentScan("com.boraji.tutorial.spring.dao"),
	      @ComponentScan("com.boraji.tutorial.spring.service") })
public class UserPersistenceControllerServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	@PersistenceContext	
	PersonService personService;
	

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("inside the doPost ");
		 EntityManagerFactory factory = Persistence.createEntityManagerFactory("hbPU");
		 EntityManager em = factory.createEntityManager();
		 em.getTransaction().begin();	
		 em.persist(new Person("Sunil", "Bora", "suni.bora@example.com"));
		 em.persist(new Person("David", "Miller", "david.miller@example.com"));			
		 em.persist(new Person("Sameer", "Singh", "sameer.singh@example.com"));
		 em.persist(new Person("Paul", "Smith", "paul.smith@example.com"));
			
		 em.getTransaction().commit();

         em.close();
      
      response.sendRedirect("Success");
   }

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
