>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BEGIN<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

******************Please follow below steps carefully to deploy database and run Sample application.

*About Sample Application:
Please extract the ZIP File "JPAPersistenceProject.zip" and follow the below steps:-

**Sample application 'JPAPersistenceProject.war' is 'JPA' type sample application written in Java. This application have relationship with 1 databases 'portal'. 'portal' is MySql databases. 

*Requirements to run Sample Application:
 1. Tomcat 8.X must be installed on machine.
 2. MySql server must be installed on machine
 3. Download 7-Zip software to extract Sample application war file (To download please follow this link https://www.7-zip.org/download.html)

*Install and Run Application and deploy databases:

>>>>>>Before runn sample application you must run databases scripts (i.e. follow Deploy Database steps.) After that you need to make some changes in configuration files of sample application to pass your database server details to make application connection with database.

**Deploy Database:

  1. Script file is provided inside 'Portal_DB_MYSQL' folder.
  2. Just extract the script and run this script on your Mysql server.
  3. Script will create 1 database 'portal' on your database server.

    
**Change 'persistance.xml' file and provide valid database details to establist connections.   
  1. To make changes in 'persistance.xml' file right click on 'JPAPersistenceProject.war' file and then navigate to '7-Zip' and then on right side menu click on 'Open archive'.
  2. In new window click on 'WEB-INF' and then click on 'classes' folder. Expand 'META-INF' folder.
  3. Inside classes folder right click on 'persistance.xml' file.
  4. Replace jdbc connection details for database with your valid databases server details along with username and Password. 


>>>>>For example in Application.properties files you will see below steps:


<properties>
      <property name="javax.persistence.jdbc.driver" value="com.mysql.jdbc.Driver" />
      <property name="javax.persistence.jdbc.url" value="jdbc:mysql://10.191.74.90:3306/portal" />
      <property name="javax.persistence.jdbc.user" value="root" />
      <property name="javax.persistence.jdbc.password" value="root123" />
      <property name="hibernate.show_sql" value="true" />
      <property name="hibernate.hbm2ddl.auto" value="update" />
    </properties>
 

Now you need to change mysql server details (i.e. IP and Port Number). Database username and Password. No need to change database name 'portal'.


**Install Application:

  1. This application will run on Apache Tomcat server. (To run Tomcat you must have Java 8 installed on machine.)
  2. Copy 'JPAPersistenceProject.war' file and paste it in Webapps folder of Apache Tomcat server.
  3. Run Tomcat 8 server. 

  
**Verify application is running or not?

  1. To do this you need to below URL in your browser:
  
     http://{your IP Address}:Port Number on which Tomcat is running/JPAPersistenceProject/
  
     For example in my case my tomcat is running on port 8080 and my machine IP was 10.191.74.90. So, to ensure application is running I hit below URL in browser: 
   
     http://10.191.74.90:8080/JPAPersistenceProject/

  2. Enter values in UI and click on 'Submit' button. Please make sure enter PersonId is numeric number.




>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>END<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  


