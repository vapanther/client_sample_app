CREATE DATABASE  IF NOT EXISTS `portal` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `portal`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: portal
-- ------------------------------------------------------
-- Server version	5.5.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `persons`
--

DROP TABLE IF EXISTS `persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persons` (
  `FIRST_NAME` varchar(150) DEFAULT NULL,
  `LAST_NAME` varchar(150) DEFAULT NULL,
  `EMAIL` varchar(150) DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persons`
--

LOCK TABLES `persons` WRITE;
/*!40000 ALTER TABLE `persons` DISABLE KEYS */;
INSERT INTO `persons` VALUES ('Sunil','Bora','suni.bora@example.com',53),('David','Miller','david.miller@example.com',54),('Sameer','Singh','sameer.singh@example.com',55),('Paul','Smith','paul.smith@example.com',56),('Sunil','Bora','suni.bora@example.com',57),('David','Miller','david.miller@example.com',58),('Sameer','Singh','sameer.singh@example.com',59),('Paul','Smith','paul.smith@example.com',60),('Sunil','Bora','suni.bora@example.com',61),('David','Miller','david.miller@example.com',62),('Sameer','Singh','sameer.singh@example.com',63),('Paul','Smith','paul.smith@example.com',64),('Sunil','Bora','suni.bora@example.com',65),('David','Miller','david.miller@example.com',66),('Sameer','Singh','sameer.singh@example.com',67),('Paul','Smith','paul.smith@example.com',68),('Sunil','Bora','suni.bora@example.com',69),('David','Miller','david.miller@example.com',70),('Sameer','Singh','sameer.singh@example.com',71),('Paul','Smith','paul.smith@example.com',72),('Sunil','Bora','suni.bora@example.com',73),('David','Miller','david.miller@example.com',74),('Sameer','Singh','sameer.singh@example.com',75),('Paul','Smith','paul.smith@example.com',76),('Sunil','Bora','suni.bora@example.com',77),('David','Miller','david.miller@example.com',78),('Sameer','Singh','sameer.singh@example.com',79),('Paul','Smith','paul.smith@example.com',80),('Sunil','Bora','suni.bora@example.com',81),('David','Miller','david.miller@example.com',82),('Sameer','Singh','sameer.singh@example.com',83),('Paul','Smith','paul.smith@example.com',84),('Sunil','Bora','suni.bora@example.com',85),('David','Miller','david.miller@example.com',86),('Sameer','Singh','sameer.singh@example.com',87),('Paul','Smith','paul.smith@example.com',88),('Sunil','Bora','suni.bora@example.com',89),('David','Miller','david.miller@example.com',90),('Sameer','Singh','sameer.singh@example.com',91),('Paul','Smith','paul.smith@example.com',92);
/*!40000 ALTER TABLE `persons` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-22 11:29:09
