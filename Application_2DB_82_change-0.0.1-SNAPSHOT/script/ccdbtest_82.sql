CREATE DATABASE  IF NOT EXISTS `ccdbtest_82` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ccdbtest_82`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: 10.191.74.82    Database: ccdbtest_82
-- ------------------------------------------------------
-- Server version	5.5.57-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` char(60) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `date_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Ankit','Kumar Tripathi','akt@crosscode.io','$2a$10$PjdNLoGZdyZ78e06halSX.lGjFIT/4abaNL1Usq7HDd5yzHbBUbga','','Crosscode','2016-10-16 12:55:01'),(2,'Vikram','Bakshi','vik@crosscode.io','$2a$10$BR6Y33nc1RPZ6u7ai9WBpeaxDuwIe7Up2n9.NQIz1vtF6E7Ql0ZPG','','crosscode','2016-10-17 00:17:59'),(3,'Mahesh','Kumar','mahesh@crosscode.io','$2a$10$4uH3Didm2bjcaWPbOisnl.8toIgtPdB8oVoQPYgpRlYumlARUO2uS','','Crosscode','2016-12-12 00:15:20'),(4,'Rajendra','Kumar','aa@gmail.com','$2a$10$U7IgzhII.twgqZivLIBHMuxgvbYxlqwGuUWGD4ikaoE5bAnTv0Np.','','Crosscode','2016-12-29 00:15:24'),(5,'Ajay','Singh','ajay@crsscode.io','$2a$10$bObY3MH7ITxAR0qxoTWPOu2AzsCMCaXCeW/5wL0N7gZpvjvKk/Mna','','Crosscode','2017-04-20 06:21:27');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-22 11:21:46
