>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BEGIN<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

******************Please follow below steps carefully to deploy database and run Sample application.

*About Sample Application:

Please extract the ZIP File "App2DB82_script.zip" and follow the below steps:-

**Sample application 'Application_2DB_82_change_DBtype-0.0.1-SNAPSHOT' is 'SpringBoot' type sample application written in Java. This application have relationship with 2 databases 'ccdbtest_82' and 'ccdbaccounts_82'. Both are 
  MySql databases. 

*Requirements to run Sample Application:
 1. Tomcat 8.X must be installed on machine.
 2. MySql server must be installed on machine
 3. Download 7-Zip software to extract Sample application war file (To download please follow this link https://www.7-zip.org/download.html)

*Install and Run Application and deploy databases:

>>>>>>Before runn sample application you must run databases scripts (i.e. follow Deploy Database steps.) After that you need to make some changes in configuration files of sample application to pass your database server details to make application connection with database.

**Deploy Database:

  1. Script file is provided inside 'App2DB82_script' folder.
  2. Just extract the script and run this script on your Mysql server.
  3. Script will create 2 databases 'ccdbtest_82' and 'ccdbaccounts_82'.

    
**Change 'Application.properties' files and provide valid database details to establist connections.   
  1. To make changes in 'Application.properties' files right click on 'Application_2DB_82_change_DBtype-0.0.1-SNAPSHOT.war' file and then navigate to '7-Zip' and then on right side menu click on 'Open archive'.
  2. In new window click on 'WEB-INF' and then click on 'classes' folder.
  3. Inside classes folder right click on 'Application.properties' file.
  4. Replace jdbc connection details for both the databases with your valid databases server details along with username and Password. Do this for all instances in Application.properties file.


>>>>>For example in Application.properties files you will see below steps:


spring.datasource.url=jdbc:mysql://10.191.74.90:3306/ccdbtest_82?autoReconnect=true&useSSL=false
spring.datasource.username=root
spring.datasource.password=root123
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.datasource.testOnBorrow: true
spring.datasource.validationQuery: SELECT 1
 

Now you need to change mysql server details (i.e. IP and Port Number). Database username and Password. No need to change database name 'ccdbtest_82' and 'ccdbAccounts_82'.


**Install Application:

  1. This application will run on Apache Tomcat server. (To run Tomcat you must have Java 8 installed on machine.)
  2. Copy 'Application_2DB_82_change_DBtype-0.0.1-SNAPSHOT.war' file and paste it in Webapps folder of Apache Tomcat server.
  3. Run Tomcat 8 server. 

  
**Verify application is running or not?

  1. To do this you need to below URL in your browser:
  
     http://{your IP Address}:Port Number on which Tomcat is running/Application_2DB_82_change_DBtype-0.0.1-SNAPSHOT/
  
     For example in my case my tomcat is running on port 8080 and my machine IP was 10.191.74.90. So, to ensure application is running I hit below URL in browser: 
   
     http://10.191.74.90:8080/Application_2DB_82_change_DBtype-0.0.1-SNAPSHOT/

  2. Enter values in UI and click on 'Submit' button. Please make sure enter salary is numeric number.


Install CsPlugin:
1. Download the exe file.
2. Right click on  Installer and click on 'Run as Administrator'.

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>END<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  


